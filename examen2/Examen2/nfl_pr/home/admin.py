from django.contrib import admin
from .models import Equipo, Ciudad, Estadio
# Register your models here.

@admin.register(Equipo)
class EquipoAdmin(admin.ModelAdmin):
    list_display = [
        "id_equipo",
        "name",
        "city",
        "stadium"
    ]

@admin.register(Ciudad)
class CiudadAdmin(admin.ModelAdmin):
    list_display = [
        "id_ciudad",
        "name"
    ]

@admin.register(Estadio)
class EstadioAdmin(admin.ModelAdmin):
    list_display = [
        "id_estadio",
        "name"
    ]