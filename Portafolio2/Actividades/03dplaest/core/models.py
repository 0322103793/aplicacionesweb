from django.db import models

from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver
from datetime import timedelta

# Create your models here.

STATE_CHOICES = (
    ('Done', 'DN' ),
    ('Doing', 'DG'),
    ('Not Stared', 'NS'),
)


class GrantGoal(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    grantgoal_name = models.CharField(max_length=64, default="Generic Grant Goal")
    description = models.CharField(max_length=256, default="Generic Grant Goal Description")
    timestamp = models.DateField(auto_now_add=True)
    final_date = models.DateField(auto_now_add=False, blank=True, null=True)
    update_date = models.DateField(auto_now=True)
    days_duration = models.IntegerField(default=7)
    status = models.BooleanField(default=True)
    state = models.CharField(max_length=16, choices=STATE_CHOICES)
    slug = models.SlugField(max_length=16)

    def __str__(self):
        return self.grantgoal_name

@receiver(post_save, sender=GrantGoal)
def end_time_grantgoal(sender, instance, **kwargs):
    if instance.final_date is None or instance.final_date=='':
        instance.final_date = instance.timestamp + timedelta(days=instance.days_duration)
        instance.save()

# @receiver(post_save, sender=GrantGoal)
# def end_time_grantgoal(sender, instance, **kwargs):
#     if instance.final_date is None or instance.final_date=='':
#         instance.final_date = instance.timestamp + timedelta(days=instance.days_duration)
#         instance.save()