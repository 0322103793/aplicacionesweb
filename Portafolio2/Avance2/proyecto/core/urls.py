from django.urls import path
from core import views

app_name = 'core'

urlpatterns = [
    path('', views.Index.as_view(), name="index"),
    
    path('list/Areas-de-Trabajo/', views.AreasTrabajo.as_view(), name="AreasTrabajo"),
    path('Detail/Areas-de-Trabajo/<int:pk>/', views.DetailAreasTrabajo.as_view(), name='DetailAT'),
    path('Create/Areas-de-Trabajo/', views.CreateAreaTrabajo.as_view(), name='CreateAT'),
    path('Update/Areas-de-Trabajo/<int:pk>/', views.UpdateAreaTrabajo.as_view(), name='UpdateAT'),
    path('Delete/Areas-de-Trabajo/<int:pk>/', views.DeleteAreaTrabajo.as_view(), name='DeleteAT'),

    path('list/Empleados/', views.Empleado.as_view(), name="Empleados"),
    path('Detail/Empleado/<int:pk>/', views.DetailEmpleado.as_view(), name='DetailE'),
    path('Create/Empleado/', views.CreateEmpleado.as_view(), name='CreateE'),
    path('Update/Empleado/<int:pk>/', views.UpdateEmpleado.as_view(), name='UpdateE'),
    path('Delete/Empleado/<int:pk>/', views.DeleteEmpleado.as_view(), name='DeleteE'),

    path('list/Bitacora/', views.Bitacora.as_view(), name="Bitacora"),
    path('Detail/Bitacora/<int:pk>/', views.DetailBitacora.as_view(), name='DetailBi'),
    path('Create/Bitacora/', views.CreateBitacora.as_view(), name='CreateBi'),
    path('Update/Bitacora/<int:pk>/', views.UpdateBitacora.as_view(), name='UpdateBi'),
    path('Delete/Bitacora/<int:pk>/', views.DeleteBitacora.as_view(), name='DeleteBi'),
    
]
