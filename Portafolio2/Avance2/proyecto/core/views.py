from django.shortcuts import render
from django.urls import reverse_lazy
from django.views import generic
from . import models
from . import forms

# Create your views here.
class Index(generic.View):
    template_name = 'core/Index.html'
    context = {}
    def get(self, request):
        self.context = {
            "Areas": models.Areasdetrabajo.objects.all(),
            "Empleados": models.Empleados.objects.all(),
            "Bitacoras": models.Bitacoras.objects.all()
        }
        return render(request, self.template_name, self.context)


# Areas de trabajos

class AreasTrabajo(generic.ListView):
    template_name = 'core/Areas_Trabajo/Area_Trabajo.html'
    model = models.Areasdetrabajo
    
    
    
class DetailAreasTrabajo(generic.DetailView):
    model=models.Areasdetrabajo
    template_name='core/Areas_Trabajo/Detail_Area_Trabajo.html'
    context = {}
    def get(self, request, pk):
        areaTrabajo = models.Areasdetrabajo.objects.get(numero=pk)
        self.context = {
            "Area": areaTrabajo
        }
        return render(request,  self.template_name, self.context)
    
class CreateAreaTrabajo(generic.CreateView):
    template_name = 'core/Areas_Trabajo/Create_Area_Trabajo.html'
    model = models.Areasdetrabajo
    form_class = forms.CreateAreaTrabajo
    success_url = reverse_lazy('core:AreasTrabajo')
    
class UpdateAreaTrabajo(generic.UpdateView):
    template_name = 'core/Areas_Trabajo/Update_Area_Trabajo.html'
    model = models.Areasdetrabajo
    form_class = forms.UpdateAreaTrabajo
    success_url = reverse_lazy("core:AreasTrabajo")

class DeleteAreaTrabajo(generic.DeleteView):
    template_name = 'core/Areas_Trabajo/Delete_Area_Trabajo.html'
    model = models.Areasdetrabajo
    success_url = reverse_lazy("core:AreasTrabajo")
    
    
    

# Empleados

class Empleado(generic.ListView):
    template_name = 'core/Empleados/Empleados.html'
    model = models.Empleados
    
    
       
class DetailEmpleado(generic.DetailView):
    model=models.Empleados
    template_name='core/Empleados/Detail_Empleado.html'
    context = {}
    def get(self, request, pk):
        empleado = models.Empleados.objects.get(numero=pk)
        self.context = {
            "Empleado": empleado
        }
        return render(request,  self.template_name, self.context)   
    
class CreateEmpleado(generic.CreateView):
    template_name = 'core/Empleados/Create_Empleado.html'
    model = models.Empleados
    form_class = forms.CreateEmpleado
    success_url = reverse_lazy('core:Empleados')
    
class UpdateEmpleado(generic.UpdateView):
    template_name = 'core/Empleados/Update_Empleado.html'
    model = models.Empleados
    form_class = forms.UpdateEmpleado
    success_url = reverse_lazy("core:Empleados")

class DeleteEmpleado(generic.DeleteView):
    template_name = 'core/Empleados/Delete_Empleado.html'
    model = models.Empleados
    success_url = reverse_lazy("core:Empleados")
    
    
    
# Bitacoras

class Bitacora(generic.ListView):
    template_name = 'core/Bitacoras/Bitacora.html'
    model = models.Bitacoras
    
    
       
class DetailBitacora(generic.DetailView):
    model=models.Bitacoras
    template_name='core/Bitacoras/Detail_Bitacora.html'
    context = {}
    def get(self, request, pk):
        bitacora = models.Bitacoras.objects.get(numero=pk)
        self.context = {
            "Bitacora": bitacora
        }
        return render(request,  self.template_name, self.context)
    
class CreateBitacora(generic.CreateView):
    template_name = 'core/Bitacoras/Create_Bitacora.html'
    model = models.Bitacoras
    form_class = forms.CreateBitacora
    success_url = reverse_lazy('core:Bitacora')
    
class UpdateBitacora(generic.UpdateView):
    template_name = 'core/Bitacoras/Update_Bitacora.html'
    model = models.Bitacoras
    form_class = forms.UpdateBitacora
    success_url = reverse_lazy("core:Bitacora")

class DeleteBitacora(generic.DeleteView):
    template_name = 'core/Bitacoras/Delete_Bitacora.html'
    model = models.Bitacoras
    success_url = reverse_lazy("core:Bitacora")


    
    
    