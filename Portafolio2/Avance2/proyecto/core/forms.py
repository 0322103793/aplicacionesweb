from django import forms
from . import models
# Areas de Trabajos
class CreateAreaTrabajo(forms.ModelForm):
    class Meta:
        model = models.Areasdetrabajo
        fields = [
            'nombre'
        ] 
        
        widgets = {
            'nombre': forms.TextInput(attrs={'class': 'form-control'}),
        }



class UpdateAreaTrabajo(forms.ModelForm):
    class Meta:
        model = models.Areasdetrabajo
        fields = [
            'nombre'
        ] 
        widgets = {
            'nombre': forms.TextInput(attrs={'class': 'form-control'}),
        }
        
        
# Empleados

class CreateEmpleado(forms.ModelForm):
    class Meta:
        model = models.Empleados
        fields = [
            'nombrepila',
            'primerapellido',
            'segundoapellido',
            'dircalle',
            'dirnumero',
            'dircolonia',
            'numtel',
            'horario',
            'jefearea',
            'areadetrabajo',
            'puesto',
        ]

        widgets = {
            'nombrepila': forms.TextInput(attrs={'class': 'form-control', "placeholder":"Escribe el nombre de pila"}),
            'primerapellido': forms.TextInput(attrs={'class': 'form-control', "placeholder":"Escribe el primer apellido"}),
            'segundoapellido': forms.TextInput(attrs={'class': 'form-control', "placeholder":"Escribe el segundo apellido"}),
            'dircalle': forms.TextInput(attrs={'class': 'form-control', "placeholder":"Escribe el nombre de la calle"}),
            'dirnumero': forms.TextInput(attrs={'class': 'form-control', "placeholder":"Escribe el numero de direccion"}),
            'dircolonia': forms.TextInput(attrs={'class': 'form-control', "placeholder":"Escribe el nombre de la colonia"}),
            'numtel': forms.TextInput(attrs={'class': 'form-control', "placeholder":"Escribe el numero de telefono"}),
            'horario': forms.TimeInput(attrs={'class': 'form-control', "placeholder":"ingresa el horario"}),
            'jefearea': forms.Select(attrs={'class': 'form-control', "placeholder":" ingresa el jefe de area"}),
            'areadetrabajo': forms.Select(attrs={'class': 'form-control', "placeholder":"ingresa el area de trabajo"}),
            'puesto': forms.Select(attrs={'class': 'form-control', "placeholder":"ingresa el puesto"}),
        }



class UpdateEmpleado(forms.ModelForm):
    class Meta:
        model = models.Empleados
        fields = [
            'nombrepila',
            'primerapellido',
            'segundoapellido',
            'dircalle',
            'dirnumero',
            'dircolonia',
            'numtel',
            'horario',
            'jefearea',
            'areadetrabajo',
            'puesto',
        ]

        widgets = {
            'nombrepila': forms.TextInput(attrs={'class': 'form-control', "placeholder":"Escribe el nombre de pila"}),
            'primerapellido': forms.TextInput(attrs={'class': 'form-control', "placeholder":"Escribe el primer apellido"}),
            'segundoapellido': forms.TextInput(attrs={'class': 'form-control', "placeholder":"Escribe el segundo apellido"}),
            'dircalle': forms.TextInput(attrs={'class': 'form-control', "placeholder":"Escribe el nombre de la calle"}),
            'dirnumero': forms.TextInput(attrs={'class': 'form-control', "placeholder":"Escribe el numero de direccion"}),
            'dircolonia': forms.TextInput(attrs={'class': 'form-control', "placeholder":"Escribe el nombre de la colonia"}),
            'numtel': forms.TextInput(attrs={'class': 'form-control', "placeholder":"Escribe el numero de telefono"}),
            'horario': forms.TimeInput(attrs={'class': 'form-control', "placeholder":"ingresa el horario"}),
            'jefearea': forms.Select(attrs={'class': 'form-control', "placeholder":" ingresa el jefe de area"}),
            'areadetrabajo': forms.Select(attrs={'class': 'form-control', "placeholder":"ingresa el area de trabajo"}),
            'puesto': forms.Select(attrs={'class': 'form-control', "placeholder":"ingresa el puesto"}),
        }
        
# Bitacoras

class CreateBitacora(forms.ModelForm):
    class Meta:
        model = models.Bitacoras
        fields = [
            'titulo',
            'empleado',
            'metas',
        ] 
        
        widgets = {
            'titulo': forms.TextInput(attrs={'class': 'form-control'}),
            'empleado': forms.Select(attrs={'class': 'form-control'}),
            'metas': forms.Select(attrs={'class': 'form-control'}),
        }



class UpdateBitacora(forms.ModelForm):
    class Meta:
        model = models.Bitacoras
        fields = [
            'titulo',
            'empleado',
            'metas',
        ] 
        
        widgets = {
            'titulo': forms.TextInput(attrs={'class': 'form-control'}),
            'empleado': forms.Select(attrs={'class': 'form-control'}),
            'metas': forms.Select(attrs={'class': 'form-control'}),
        }
        

