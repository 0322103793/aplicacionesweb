# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey and OneToOneField has `on_delete` set to the desired behavior
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from django.db import models


class AtModelos(models.Model):
    area = models.ForeignKey('Areasdetrabajo', models.DO_NOTHING, db_column='Area')  # Field name made lowercase.
    modelo = models.ForeignKey('Modelos', models.DO_NOTHING, db_column='Modelo')  # Field name made lowercase.
    cantidad = models.IntegerField(db_column='Cantidad')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'AT_Modelos'


class AtPiezas(models.Model):
    area = models.OneToOneField('Areasdetrabajo', models.DO_NOTHING, db_column='Area', primary_key=True)  # Field name made lowercase. The composite primary key (Area, Pieza) found, that is not supported. The first column is selected.
    pieza = models.ForeignKey('Piezas', models.DO_NOTHING, db_column='Pieza')  # Field name made lowercase.
    cantidadpieza = models.IntegerField(db_column='CantidadPieza')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'AT_Piezas'
        unique_together = (('area', 'pieza'),)


class Actividades(models.Model):
    numero = models.AutoField(primary_key=True)
    descripcion = models.CharField(db_column='Descripcion', max_length=100, blank=True, null=True)  # Field name made lowercase.
    estado = models.ForeignKey('Estados', models.DO_NOTHING, db_column='Estado', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'Actividades'


class Areasdetrabajo(models.Model):
    numero = models.AutoField(primary_key=True)
    nombre = models.CharField(db_column='Nombre', unique=True, max_length=30)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'AreasDeTrabajo'


class BitAct(models.Model):
    bitacora = models.ForeignKey('Bitacoras', models.DO_NOTHING, db_column='Bitacora')  # Field name made lowercase.
    actividad = models.ForeignKey(Actividades, models.DO_NOTHING, db_column='Actividad')  # Field name made lowercase.
    tiempo = models.TimeField(db_column='Tiempo')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'Bit_Act'


class BitTarea(models.Model):
    bitacora = models.ForeignKey('Bitacoras', models.DO_NOTHING, db_column='Bitacora')  # Field name made lowercase.
    tarea = models.ForeignKey('Tareas', models.DO_NOTHING, db_column='Tarea')  # Field name made lowercase.
    fechainicio = models.DateField(db_column='FechaInicio')  # Field name made lowercase.
    fechafinal = models.DateField(db_column='FechaFinal', blank=True, null=True)  # Field name made lowercase.
    horainicio = models.TimeField(db_column='HoraInicio')  # Field name made lowercase.
    horafinal = models.TimeField(db_column='HoraFinal', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'Bit_Tarea'


class Bitacoras(models.Model):
    numero = models.AutoField(primary_key=True)
    titulo = models.CharField(db_column='Titulo', max_length=30)  # Field name made lowercase.
    empleado = models.ForeignKey('Empleados', models.DO_NOTHING, db_column='Empleado', blank=True, null=True)  # Field name made lowercase.
    metas = models.ForeignKey('Metas', models.DO_NOTHING, db_column='metas', blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'Bitacoras'


class Empleados(models.Model):
    numero = models.AutoField(primary_key=True)
    nombrepila = models.CharField(db_column='NombrePila', max_length=30)  # Field name made lowercase.
    primerapellido = models.CharField(db_column='PrimerApellido', max_length=30)  # Field name made lowercase.
    segundoapellido = models.CharField(db_column='SegundoApellido', max_length=30, blank=True, null=True)  # Field name made lowercase.
    dircalle = models.CharField(db_column='DirCalle', max_length=30)  # Field name made lowercase.
    dirnumero = models.CharField(db_column='DirNumero', max_length=30)  # Field name made lowercase.
    dircolonia = models.CharField(db_column='DirColonia', max_length=30)  # Field name made lowercase.
    numtel = models.CharField(db_column='NumTel', max_length=15)  # Field name made lowercase.
    horario = models.TimeField(db_column='Horario', blank=True, null=True)  # Field name made lowercase.
    jefearea = models.ForeignKey('self', models.DO_NOTHING, db_column='JefeArea', blank=True, null=True)  # Field name made lowercase.
    areadetrabajo = models.ForeignKey(Areasdetrabajo, models.DO_NOTHING, db_column='AreaDeTrabajo', blank=True, null=True)  # Field name made lowercase.
    puesto = models.ForeignKey('Puestos', models.DO_NOTHING, db_column='Puesto', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'Empleados'


class Errores(models.Model):
    numero = models.AutoField(primary_key=True)
    descripcion = models.CharField(db_column='Descripcion', max_length=100)  # Field name made lowercase.
    bitacora = models.ForeignKey(Bitacoras, models.DO_NOTHING, db_column='Bitacora', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'Errores'


class Estados(models.Model):
    numero = models.AutoField(primary_key=True)
    descripcion = models.CharField(db_column='Descripcion', max_length=100)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'Estados'


class Materiales(models.Model):
    numero = models.AutoField(primary_key=True)
    nombre = models.CharField(db_column='Nombre', max_length=30)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'Materiales'


class Metas(models.Model):
    numero = models.AutoField(primary_key=True)
    meta = models.CharField(db_column='Meta', max_length=100)  # Field name made lowercase.
    fechainicio = models.DateField(db_column='FechaInicio')  # Field name made lowercase.
    fechafinal = models.DateField(db_column='FechaFinal')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'Metas'


class ModeloPiezas(models.Model):
    modelo = models.ForeignKey('Modelos', models.DO_NOTHING, db_column='Modelo')  # Field name made lowercase.
    pieza = models.ForeignKey('Piezas', models.DO_NOTHING, db_column='Pieza')  # Field name made lowercase.
    totalpiezas = models.IntegerField(db_column='TotalPiezas')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'Modelo_Piezas'


class Modelos(models.Model):
    numero = models.AutoField(primary_key=True)
    nombre = models.CharField(db_column='Nombre', max_length=30)  # Field name made lowercase.
    descripcion = models.CharField(db_column='Descripcion', max_length=100, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'Modelos'


class Piezas(models.Model):
    numero = models.AutoField(primary_key=True)
    nombre = models.CharField(db_column='Nombre', max_length=30)  # Field name made lowercase.
    stock = models.IntegerField(db_column='Stock')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'Piezas'


class PiezasMaterial(models.Model):
    pieza = models.ForeignKey(Piezas, models.DO_NOTHING, db_column='Pieza', blank=True, null=True)  # Field name made lowercase.
    material = models.ForeignKey(Materiales, models.DO_NOTHING, db_column='Material', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'Piezas_Material'


class Puestos(models.Model):
    numero = models.AutoField(primary_key=True)
    nombre = models.CharField(db_column='Nombre', max_length=30)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'Puestos'


class Soluciones(models.Model):
    numero = models.AutoField(primary_key=True)
    descripcion = models.CharField(db_column='Descripcion', max_length=100)  # Field name made lowercase.
    bitacora = models.ForeignKey(Bitacoras, models.DO_NOTHING, db_column='Bitacora', blank=True, null=True)  # Field name made lowercase.
    error = models.ForeignKey(Errores, models.DO_NOTHING, db_column='Error', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'Soluciones'


class Tareas(models.Model):
    numero = models.AutoField(primary_key=True)
    orden = models.IntegerField(db_column='Orden', blank=True, null=True)  # Field name made lowercase.
    areadetrabajo = models.ForeignKey(Areasdetrabajo, models.DO_NOTHING, db_column='AreaDeTrabajo', blank=True, null=True)  # Field name made lowercase.
    estado = models.ForeignKey(Estados, models.DO_NOTHING, db_column='Estado', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'Tareas'
